This repo contains a monitoring stack with:

- Prometheus with Alertmanager and Grafana
- Graylog and Elasticsearch

To run the stack, change the user and password in .env file and create the following folder structure for peristent data:

- /monitoring_server/mongo_data
- /monitoring_server/elastic_data
- /monitoring_server/graylog_data
- /monitoring_server/prometheus_data
- /monitoring_server/grafana_data

Make sure the container users have access to the files in this repository (./alertmanager , ./grafana , ./prometeus).

To configure alertmanager to send messages via slack, modify ./alertmanager/config.yml with your details, or add an email receiver.

To configure Prometheus alerts, change the ./prometheus/alert.rules file.

Prometheus user/password is admin/admin, you should change that by generating a SHA2 Hash password, you can use an online generator for that.
Grafana user/password is in the .env file, you should change the password with something strong.